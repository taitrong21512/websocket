package com.example.demo;

import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.listener.DataListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
	Configuration config = new Configuration();
	config.setHostname("localhost");
	config.setPort(9092);
	SocketIOServer server = new SocketIOServer(config);
	server.addEventListener("chat", Message.class, new DataListener<Message>() {
		@Override
		public void onData(SocketIOClient socketIOClient, Message message, AckRequest ackRequest) throws Exception {
			server.getBroadcastOperations().sendEvent("chat",message);
		}
	});

		SpringApplication.run(DemoApplication.class, args);
	}

}
